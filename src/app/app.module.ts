import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NgSemanticModule } from 'ng-semantic';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [BrowserModule, NgSemanticModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
